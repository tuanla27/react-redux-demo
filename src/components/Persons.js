import React, { Component } from 'react';
import { connect } from "react-redux";

class Persons extends Component {
  render() {
    return (
      <div className='post'>
        <h3 className='post_title'>{this.props.person.name}</h3>
        <p className='post_message'>{this.props.person.company}</p>
        <div className='control-buttons'>
          <button className="edit"
                  onClick={() => this.props.dispatch({type: 'EDIT_PERSON', data: this.props.person})}>
            Edit
          </button>
          <button className='delete'
            onClick={() => this.props.dispatch({type: 'DELETE_PERSON', id: this.props.person._id})}>
            Delete
          </button>
        </div>
      </div>
    );
  }
}

export default connect()(Persons);