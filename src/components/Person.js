import React, { Component } from 'react';
import { connect } from "react-redux";

class Person extends Component {
  handleSubmit = (e) => {
    e.preventDefault();
    const name = this.getName.value;
    const company =  this.getCompany.value;
    const data = {
      name,
      company,
      edited:false
    };
    console.log(data);
    this.props.dispatch({
      type:'ADD_PERSON',
      data});
    this.getName.value = '';
    this.getCompany.value = '';
  };
  render() {
    return (
      <div className='post-container'>
        <h1 className='post_heading'>Create Person</h1>
        <form className='form' onSubmit={this.handleSubmit}>
          <input required type="text" ref={(input)=>this.getName = input}
                 placeholder="Enter Name"/>
          <br /><br />
          <input required type="text" ref={(input)=>this.getCompany = input}
                 placeholder="Enter Company"/>
          <br /><br />
          <button>Create</button>
        </form>
      </div>
    );
  }
}
export default connect()(Person);