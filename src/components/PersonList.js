
import React, { Component } from 'react';
import { connect } from "react-redux";
import Persons from "./Persons";

class PersonList extends Component {
  constructor() {
    super();
    this.state = {
      persons: []
    }
  }
  componentDidMount() {
    fetch('http://localhost:2707/persons', {method: 'GET', mode: "cors"})
      .then(result => {
        console.log(result);
        return result.json();
      }).then(data => {
      this.setState({persons: data});
      console.log(this.state, data);
    });
  }

  render() {
    return (
      <div>
        <h1 className='post_heading'>All Persons</h1>
        {this.state.persons.map((person) => (
          <div key={person._id}>
              <Persons key={person._id} person={person} />
          </div>
        ))}

      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    persons: state
  }
};
export default connect(mapStateToProps)(PersonList);