import React, { Component } from 'react';
import { connect } from 'react-redux';


class UpdatePerson extends Component {
  handleEdit = (e) => {
    e.preventDefault();
    const newName = this.getName.value;
    const newCompany = this.getCompany.value;
    const data = {
      newName,
      newCompany
    }
    // this.props.dispatch({ type: 'UPDATE', id: this.props.persons._id, data: data })
  }
  render() {
    return (
      <div key={this.props.persons._id} className="post">
        <form className='form' onSubmit={this.handleEdit}>
          <input required type="text" ref={(input) => this.getName = input}
                 defaultValue={this.props.persons.name} /><br /><br />
          <textarea required rows="5" ref={(input) => this.getCompany = input}
                    defaultValue={this.props.persons.company} cols="28" /><br /><br />
          <button>Update</button>
        </form>
      </div>
    );
  }
}
export default connect()(UpdatePerson);