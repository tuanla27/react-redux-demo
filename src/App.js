import React, { Component } from 'react';
import Person from "./components/Person";
import PersonList from "./components/PersonList";

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="navbar">
          <h2 className="center ">Persons Center</h2>
        </div>
        <Person />
        <PersonList />
      </div>
    );
  }
}

export default App;
