import queryString from 'query-string';

const personReducer = (state = [], action) => {
  switch (action.type) {
    case 'ADD_PERSON':
      return fetch('http://localhost:2707/persons',
        {
          method: 'POST',
          mode: "cors",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          body: queryString.stringify(action.data)
        })
        .then();
    case 'DELETE_PERSON':
      return fetch(`http://localhost:2707/person/${action.id}`,
        {
          method: 'DELETE',
          mode: "cors"
        }
      )
        .then();
    case 'EDIT_PERSON':
      return state.map((person)=>person._id === action.data._id ? {...person,edited:!person.edited}:person);
    case 'UPDATE':
      return fetch(`http://localhost:2707/person/${action.data._id}`,
        {
          method: 'PUT',
          mode: "cors",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          body: queryString.stringify(action.data)
        })
        .then();
    default:
      return state;
  }
};
export default personReducer;